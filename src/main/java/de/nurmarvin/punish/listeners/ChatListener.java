package de.nurmarvin.punish.listeners;

import com.j256.ormlite.stmt.QueryBuilder;
import de.nurmarvin.punish.Punish;
import de.nurmarvin.punish.misc.PunishUser;
import de.nurmarvin.punish.misc.PunishUtils;
import de.nurmarvin.punish.misc.Punishment;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChatListener implements Listener {
    @EventHandler
    public void onChat(ChatEvent event) {
        ProxiedPlayer player = (ProxiedPlayer) event.getSender();
        QueryBuilder<PunishUser, UUID> statementBuilder = Punish.getInstance().getUserDao().queryBuilder();
        try {
            statementBuilder.where().idEq(player.getUniqueId());
            List<PunishUser> players = Punish.getInstance().getUserDao().query(statementBuilder.prepare());
            PunishUser punishUser;
            if (players.size() > 0) {
                punishUser = players.get(0);
            } else {
                punishUser = new PunishUser(player.getUniqueId(), new ArrayList<>());
                Punish.getInstance().getUserDao().create(punishUser);
            }

            Punish.getInstance().getUsers().put(player.getUniqueId(), punishUser);

            Punishment activePunishment = PunishUtils.getActivePunishment(punishUser, Punishment.PunishmentType.MUTE);

            if (activePunishment != null) {
                event.setCancelled(true);
                player.sendMessage(new TextComponent(PunishUtils.getMuteMesage(activePunishment)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
