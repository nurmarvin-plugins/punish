package de.nurmarvin.punish.listeners;

import com.j256.ormlite.stmt.QueryBuilder;
import de.nurmarvin.punish.Punish;
import de.nurmarvin.punish.misc.PunishUser;
import de.nurmarvin.punish.misc.PunishUtils;
import de.nurmarvin.punish.misc.Punishment;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LoginListener implements Listener
{
    @EventHandler
    public void onLogin(LoginEvent event)
    {
        QueryBuilder<PunishUser, UUID> statementBuilder = Punish.getInstance().getUserDao().queryBuilder();
        try {
            statementBuilder.where().idEq(event.getConnection().getUniqueId());
            List<PunishUser> players = Punish.getInstance().getUserDao().query(statementBuilder.prepare());
            PunishUser punishUser;
            if(players.size() > 0)
            {
                punishUser = players.get(0);
            }
            else
            {
                punishUser = new PunishUser(event.getConnection().getUniqueId(), new ArrayList<>());
                Punish.getInstance().getUserDao().create(punishUser);
            }

            Punish.getInstance().getUsers().put(event.getConnection().getUniqueId(), punishUser);

            Punishment activePunishment = null;

            for (int i = 0; i < punishUser.getPunishments().size(); i++) {
                Punishment punishment = punishUser.getPunishmentsAsObjects().get(i);
                if (punishment.getRemovedBy() == null)
                {
                    if (punishment.getEndsOn().getTime() >= System.currentTimeMillis() || punishment.getEndsOn().getTime() == -1)
                    {
                        if (punishment.getPunishmentType() == Punishment.PunishmentType.BAN || punishment.getPunishmentType() == Punishment.PunishmentType.BLACKLIST) {
                            activePunishment = punishUser.getPunishmentsAsObjects().get(i);
                            break;
                        }
                    }
                }
            }

            if(activePunishment != null)
            {
                event.setCancelled(true);

                event.setCancelReason(new TextComponent(PunishUtils.getBanMessage(activePunishment)));
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
