package de.nurmarvin.punish.commands;

import de.nurmarvin.punish.Punish;
import de.nurmarvin.punish.misc.PunishUser;
import de.nurmarvin.punish.misc.Punishment;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Date;

public class WarnCommand extends Command {
    public WarnCommand() {
        super("warn", "punish.warn");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        Date date = new Date(-1);

        Punishment punishment = new Punishment(args[1], date, sender.getName(), Punishment.PunishmentType.WARN, null, null, null);

        ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(args[0]);

        PunishUser punishUser = Punish.getInstance().getUsers().get(proxiedPlayer.getUniqueId());

        punishUser.handlePunishment(punishment);

    }
}
