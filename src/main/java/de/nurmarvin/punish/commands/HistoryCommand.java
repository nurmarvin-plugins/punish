package de.nurmarvin.punish.commands;

import de.nurmarvin.punish.Punish;
import de.nurmarvin.punish.misc.PunishUser;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

public class HistoryCommand extends Command {
    public HistoryCommand() {
        super("history", "punish.history");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        
        if(args.length < 1)
        {
            sender.sendMessage(new TextComponent("§c/history <Player>"));
            return;
        }
        
        ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(args[0]);
        UUID uuid;
        
        if(proxiedPlayer == null)
        {
            uuid = ProxyServer.getInstance().getOfflinePlayer(args[0]).getUniqueId();
        }
        else
        {
            uuid = proxiedPlayer.getUniqueId();
        }

        PunishUser punishUser = Punish.getInstance().getUsers().get(uuid);

        punishUser.getPunishmentsAsObjects().forEach(punishment -> sender.sendMessage(new TextComponent("§cType: " + punishment.getPunishmentType() + "§c, Reason: §e" + punishment.getReason() + "§c, Ends: §e" + punishment.getEndsOn())));
    }
}
