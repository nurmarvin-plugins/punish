package de.nurmarvin.punish;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import de.nurmarvin.punish.commands.*;
import de.nurmarvin.punish.listeners.LoginListener;
import de.nurmarvin.punish.misc.FileManager;
import de.nurmarvin.punish.misc.PunishUser;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

@Getter
public final class Punish extends Plugin {
    @Getter
    private static Punish instance;
    private FileManager fileManager;
    private HashMap<UUID, PunishUser> users;

    private Dao<PunishUser, UUID> userDao;

    @Override
    public void onEnable()
    {
        instance = this;

        ProxyServer.getInstance().getPluginManager().registerListener(this, new LoginListener());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BanCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MuteCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new BlacklistCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new WarnCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new KickCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new HistoryCommand());


        this.users = new HashMap<>();
        this.fileManager = new FileManager();

        String[] mysqlData = this.fileManager.read();

        if(mysqlData.length == 0)
        {
            System.err.println("Please make sure that you have provided MySQL credentials!");
            return;
        }

        ConnectionSource connectionSource;
        try {
            connectionSource = new JdbcConnectionSource(mysqlData[0], mysqlData[1], mysqlData[2]);

            setupDatabase(connectionSource);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        try {
            userDao.getConnectionSource().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setupDatabase(ConnectionSource connectionSource) throws SQLException {

        userDao = DaoManager.createDao(connectionSource, PunishUser.class);

        TableUtils.createTableIfNotExists(connectionSource, PunishUser.class);
    }
}
