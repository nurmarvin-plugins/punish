package de.nurmarvin.punish.misc;

public class PunishUtils {
    public static String getBanMessage(Punishment punishment) {
        if (punishment.getPunishmentType() == Punishment.PunishmentType.BLACKLIST) {
            return "§cYour account was blacklisted on the network." +
                    "\n" +
                    "\n§cReason: " + punishment.getReason();
        }

        return "§cYour account was " + (punishment.getEndsOn().getTime() == -1 ? "permanently" : "temporarily") + " suspended from the network." +
                "\n" +
                "\n§cReason: " + punishment.getReason() +
                (punishment.getEndsOn().getTime() == -1 ? "" : "\n§cEnds: " + punishment.getEndsOn());
    }

    public static String getMuteMesage(Punishment punishment) {
        return "§cYour account was " + (punishment.getEndsOn().getTime() == -1 ? "permanently" : "temporarily") + " muted on the network." +
                "\n" +
                "\n§cReason: " + punishment.getReason() +
                (punishment.getEndsOn().getTime() == -1 ? "" : "\n§cEnds: " + punishment.getEndsOn());
    }

    public static Punishment getActivePunishment(PunishUser punishUser, Punishment.PunishmentType punishmentType) {
        for (int i = 0; i < punishUser.getPunishments().size(); i++) {
            Punishment punishment = punishUser.getPunishmentsAsObjects().get(i);
            if (punishment.getRemovedBy() == null) {
                if (punishment.getEndsOn().getTime() >= System.currentTimeMillis() || punishment.getEndsOn().getTime() == -1) {
                    if (punishment.getPunishmentType().equals(punishmentType)) {
                        return punishUser.getPunishmentsAsObjects().get(i);
                    }
                }
            }
        }
        return null;
    }

    static String getWarnMessage(Punishment punishment) {
        return "§cYour account received a warning." +
                "\n" +
                "\n§cReason: " + punishment.getReason();
    }

    static String getKickMessage(Punishment punishment) {
        return "§cYou have been kicked from the network." +
                "\n" +
                "\n§cReason: " + punishment.getReason();
    }
}
