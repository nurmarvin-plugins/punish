package de.nurmarvin.punish.misc;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import de.nurmarvin.punish.Punish;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@DatabaseTable(tableName = "users")
public class PunishUser
{
    @DatabaseField(id = true)
    private UUID uuid;

    @DatabaseField(dataType = DataType.SERIALIZABLE, useGetSet = true)
    private ArrayList<Punishment> punishments;

    public ArrayList<String> getPunishments() {
        ArrayList<String> result = new ArrayList<>();

        punishments.forEach(punishment -> {
            String string = punishment.getPunishmentType() + ";" + punishment.getEndsOn().getTime() + ";" + punishment.getReason() + ";" + punishment.getIssuedBy();

            if (punishment.getRemovedBy() != null) {
                string += ";" + punishment.getRemovedOn().getTime() + ";" + punishment.getRemoveReason() + ";" + punishment.getRemovedBy();
            }

            result.add(string);
        });

        return result;
    }

    public void setPunishments(ArrayList<String> punishments) {
        ArrayList<Punishment> result = new ArrayList<>();

        punishments.forEach(string -> {
            String[] punimentStrings = string.split(";");

            Punishment punishment;

            if (punimentStrings.length > 4) {
                punishment = new Punishment(punimentStrings[2], new Date(Long.valueOf(punimentStrings[1])), punimentStrings[3], Punishment.PunishmentType.valueOf(punimentStrings[0]), punimentStrings[5], new Date(Long.valueOf(punimentStrings[4])), punimentStrings[6]);
            } else {
                punishment = new Punishment(punimentStrings[2], new Date(Long.valueOf(punimentStrings[1])), punimentStrings[3], Punishment.PunishmentType.valueOf(punimentStrings[0]), null, null, null);
            }


            result.add(punishment);
        });

        this.punishments = result;
    }

    public ArrayList<Punishment> getPunishmentsAsObjects() {
        return punishments;
    }

    public void handlePunishment(Punishment punishment) {
        ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(this.uuid);
        this.getPunishmentsAsObjects().add(punishment);

        if (punishment.getPunishmentType().equals(Punishment.PunishmentType.BAN) || punishment.getPunishmentType().equals(Punishment.PunishmentType.BLACKLIST)) {

            proxiedPlayer.disconnect(new TextComponent(PunishUtils.getBanMessage(punishment)));
        } else if (punishment.getPunishmentType().equals(Punishment.PunishmentType.KICK)) {
            proxiedPlayer.disconnect(new TextComponent(PunishUtils.getKickMessage(punishment)));
        } else if (punishment.getPunishmentType().equals(Punishment.PunishmentType.MUTE)) {
            proxiedPlayer.sendMessage(new TextComponent(PunishUtils.getMuteMesage(punishment)));
        } else if (punishment.getPunishmentType().equals(Punishment.PunishmentType.WARN)) {
            proxiedPlayer.sendMessage(new TextComponent(PunishUtils.getWarnMessage(punishment)));
        }

        try {
            Punish.getInstance().getUserDao().update(this);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
