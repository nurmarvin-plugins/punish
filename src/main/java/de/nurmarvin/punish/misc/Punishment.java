package de.nurmarvin.punish.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class Punishment
{
    private String reason;
    private Date endsOn;
    private String issuedBy;
    private PunishmentType punishmentType;

    private String removeReason;
    private Date removedOn;
    private String removedBy;

    public enum PunishmentType {

        BLACKLIST,
        BAN,
        WARN,
        MUTE,
        KICK

    }
}
